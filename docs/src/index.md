# WaveguideDesigner

`WaveguideDesigner` is a package for designing waveguide refractive
index structures given a modal field. The resulting structures
may then be simulating
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
mode solving package.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMGridWaveguides.jl
```

## Features and Status

Basic functionality has been implemented:

* Direct solving for 1D index structure given modal field using scalar Helmholtz waveguide equation
* Support for targeting a modal effective index value (with no guarantee of realizable index values)
* Support bounding of waveguide index values (provided transverse mode scaling is permitted)
* Support of binary discretization of resulting index structure (resulting in more-or-less approximate mode form results)

Support for 2D structures and constrained optimization and other design
methods has not (yet?) been implemented.

It is important to note that the waveguide index structures obtained
by this package are approximations that are not necessarily practical
or even possible to implement. Sometimes the resulting structure and
supported modes are reasonable, other times not in the slightest.
Once can check the modes supported by the calculated index structure
and compare to the desired mode form using a modesolver, such as
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl).
