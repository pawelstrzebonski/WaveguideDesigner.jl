import WaveguideDesigner
import Test: @test_broken, @test, @test_throws

tests = ["mode2waveguide"]

approxeq(a, b) = all(isapprox.(a, b, rtol = 1e-6))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
